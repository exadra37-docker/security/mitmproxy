#!/bin/bash

set -eu

################################################################################
# ENV VARS
################################################################################

  # docker
  image_tag=latest
  image_name=exadra37/mitmproxy
  image_command="mitmweb --web-iface 0.0.0.0"
  #image_command="mitmweb --cert *=/etc/ssl/certs/ca-cert-ProxyCA.pem --web-iface 0.0.0.0"
  #image_command="mitmweb --set client_certs=/etc/ssl/certs/ca-cert-ProxyCA.pem --web-iface 0.0.0.0"
  #image_command="mitmweb --mode transparent --showhost --web-iface 0.0.0.0"
  docker_build_path=~/bin/vendor/exadra37-docker/security/mitmproxy/docker/build
  docker_network="host"
  background_mode=--detach
  default_port_map="-p 127.0.0.1:8080:8080 -p 127.0.0.1:8081:8081"

  # host
  mitmproxy_dir=~/.docker-mitmproxy
  profiles_dir="${mitmproxy_dir}/profiles"
  profile_dir="${profiles_dir}/default"

  env_file=~/"${profile_dir}".env.mitmproxy

  if [ -f ~/"${env_file}" ]; then
      source ~/"${env_file}"
  fi

  mkdir -p "${profile_dir}"


################################################################################
# DEFAULT VARS

################################################################################

  port_map=""
  container_name="${PWD##*/}"


################################################################################
# FUNCTIONS
################################################################################

  function Enable_Transparent_Mode_On_Host
  {
    sudo sysctl -w net.ipv4.ip_forward=1
    sudo sysctl -w net.ipv6.conf.all.forwarding=1
    sudo sysctl -w net.ipv4.conf.all.send_redirects=0
    sudo iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 8080
    sudo iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 443 -j REDIRECT --to-port 8080
    sudo ip6tables -t nat -A PREROUTING -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 8080
    sudo ip6tables -t nat -A PREROUTING -i eth0 -p tcp --dport 443 -j REDIRECT --to-port 8080
  }

  function Disable_Transparent_Mode_On_Host
  {
    printf "\n---> DISABLING TRANSPARENT MODE FOR MITMPROXY <---\n"

    sudo sysctl -w net.ipv4.ip_forward=0
    sudo sysctl -w net.ipv6.conf.all.forwarding=0
    sudo sysctl -w net.ipv4.conf.all.send_redirects=1

    # http://lubos.rendek.org/remove-all-iptables-prerouting-nat-rules/
    # https://unix.stackexchange.com/questions/405364/drop-packets-with-prerouting-in-iptables
    printf "
Manually check the PREROUTNG rules:

  $ sudo iptables -t nat --line-numbers -L

  Chain PREROUTING (policy ACCEPT)
  num  target     prot opt source               destination
  1    DOCKER     all  --  anywhere             anywhere             ADDRTYPE match dst-type LOCAL
  2    REDIRECT   tcp  --  anywhere             anywhere             tcp dpt:http redir ports 8080
  3    REDIRECT   tcp  --  anywhere             anywhere             tcp dpt:https redir ports 8080


Delete each one, but start the one with the biggest line number:

  sudo iptables -t nat -D PREROUTING 3
  sudo iptables -t nat -D PREROUTING 2

"
  }


################################################################################
# PARSE INPUT
################################################################################

  for input in "${@}"; do
    case "${input}" in

      -p | --publish )
        port_map="${port_map} -p 127.0.0.1:${2? Missing port map!!!}"
        shift 2
        ;;

      -it )
        background_mode=-it
        shift 1
        ;;

      --network )
        docker_network="${2:-${docker_network}}"
        shift 2
        ;;

      --disable-transparent-mode )
        Disable_Transparent_Mode_On_Host
        exit 0
        ;;

      --enable-transparent-mode )
        Enable_Transparent_Mode_On_Host
        shift 1
        ;;

      build )
        sudo docker build \
            -t ${image_name}:${image_tag} \
            "${docker_build_path}"

        exit 0
        ;;

      stop )
        container_name="${2:-${container_name}}"
        printf "\n---> STOP CONTAINER: ${container_name}\n"
        sudo docker container stop "${container_name}"
        exit 0
        ;;

      shell )
        container_name="${2:-${container_name}}"
        sudo docker exec -it "${container_name}" sh
        exit 0
        ;;

    esac
  done


################################################################################
# EXECUTION
################################################################################

  if [ -z "${port_map}" ]; then
    port_map="${default_port_map}"
  fi

  sudo docker run \
    --rm \
    "${background_mode}" \
    --name "${container_name}" \
    --volume "${profile_dir}":/home/mitmproxy/.mitmproxy\
    ${port_map} \
    --network "${docker_network}" \
    "${image_name}:${image_tag}" ${image_command}
